# encoding: utf-8

namespace :dev_data do
	desc "Fill database with sample data"
	task create: :environment do

		Rake::Task['db:reset'].invoke

		john = User.create!(name: 'John', email: 'john@test.com', :password => 'password')
		jack = User.create!(name: 'Jack', email: 'jack@test.com', :password => 'password')

		users = (0..110).map do |i|
			User.create!(name: "Jim #{i}", email: "jim#{i}@test.com", :password => 'password')
		end

		(0..100).each do |i|
			cloche_pied = Challenge.create!(
				title: "Cloche-pied #{i}",
				description: "Faites #{i}m à cloche-pied !",
				points: i,
				expires_at: Time.now + i.days
			)

			cloche_pied.participations.create!(
				user_id: john.id,
				message: "J'ai réussi, mais j'ai mal au pieds maintenant, c'est malin..."
			)

			users[0..i].each do |user|
				cloche_pied.participations.create!(
					user_id: user.id,
					message: "Youpi je suis #{user.name}"
				)
			end
		end

		Challenge.create!(
			title: "Ornithorynque",
			description: "Utilisez le mot ornithorynque sur votre annonce de répondeur.",
			points: 15,
			expires_at: Time.now + 10.days
		)

		(0..50).each do |i|
			baignade = Challenge.create!(
				title: "Baignade gelée #{i}",
				description: "Prendre un bain de mer en décembre",
				points: i,
				expires_at: Time.parse('31/12/2013') - i.days
			)

			users[0..i].each do |user|
				baignade.participations.create!(
					user_id: user.id,
					message: "Youpi je suis #{user.name} et je me suis baigné"
				)
			end
		end

		puts "Sample data have been set in DB !"

	end
end