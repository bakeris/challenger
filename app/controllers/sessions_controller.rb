class SessionsController < ApplicationController

	def new
	end
	
	def create
		@user = User.where(email: params[:email]).first
		if @user && @user.password == params[:password]
			session[:user_id] = @user.id
			redirect_to root_url
		else
			redirect_to new_session_path
		end
	end

	def logout
		session[:user_id] = nil
		redirect_to root_url
	end

end