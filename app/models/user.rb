require 'bcrypt'

class User < ActiveRecord::Base
	include BCrypt

	has_many :participations
	has_many :challenges, through: :participations

	paginates_per 10

	def avatar_url
		"http://www.gravatar.com/avatar/4156423#{id}?d=monsterid&s=50"
	end

	def password
	    @password ||= Password.new(password_hash)
	end

	def password=(new_password)
	    @password = Password.create(new_password)
	    self.password_hash = @password
	end

end
