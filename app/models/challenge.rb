class Challenge < ActiveRecord::Base

	has_many :participations
	has_many :users, through: :participations

	paginates_per 10

	scope :expired, -> { where("expires_at < ?", Time.now) }
	scope :running, -> { where("expires_at >= ?", Time.now) }

	scope :above_points, ->(min_points) { where("points > ?", min_points) }

end
